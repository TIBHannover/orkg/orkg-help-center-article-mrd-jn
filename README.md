# ORKG Help Center Article MRD Jupyter

Jupyter notebooks supporting the ORKG Help Center Article titled [Machine Readable by Design](https://orkg.org/help-center/article/47/Machine_readable_by_design).

[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/TIBHannover%2Forkg%2Forkg-help-center-article-mrd-jn/HEAD?labpath=example-python.ipynb)